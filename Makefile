build:
	sass src/styles/style.scss:dist/s.css
	pug src/templates/*.pug --out dist/

watch-sass:
	sass --watch src/styles/style.scss:dist/s.css

watch-pug:
	pug --watch src/templates/*.pug --out dist/

watch: watch-sass watch-pug
