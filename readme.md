# Install

A frontend működéséhez szükséges a Sass és a Pug.
A `src/styles/style.scss`-t kell lefordítani `dist/s.css`-re a Sass segítségével.
A `src/templates/*.pug`-ot, pedig a `dist/`-be azonos fájlnévvel.

__Függőségek telepítése:__
- `npm i -g sass` vagy `brew install sass` vagy más
- `npm i -g pug pug-cli`

__

# Fordítás

__1. Fordítás__

- Parancs futtatása: `make build`

__2. Watch__

- Parancs futtatása: `make -j2 watch`

__

# Futtatás

A HTML állományok a böngészőben megnyitva rendesen működnek.

__

# A látványtervtől való eltérés

Bizonyos helyeken szükséges volt eltérni a látványtervtől, mivel 1440 pixeles
felbontás alatt nem volt képes működni:

- A menüpontok betűk közti 1 pixeles eltartásának eltávolítása, valamint a konzisztencia érdekében mindenhol máshol is.
- Az asztali verzió lenyíló menüjében az eltartások csökkentése
- Skip-link alkalmazása képernyőolvasót használók számára (TODO)
- Az ikon-gombok (social, menü, footer, stb.) a látványtervben 40x40, 50x50, valamint 60x60 pixel méretűek. Az iparági ajánlások szerint az ikon-gombok 48x48 méretűek kellene, hogy legyenek (minimum), így a konzisztencia érdekében mindenhol 48x48 pixeles méretet használ a build.

A fentiek mellett további eltérésekre lenne szükség, több okból is. Az egyik fő
ok, hogy 2020. szeptember 15-én életbe lépett a hozzáférhetőségi törvény, mely
szerint a kiemelten közhasznú szervezetek weboldalai lehetőleg AA-s besorolásúak
legyenek. A másik fő ok, hogy a látványterv bizonyos helyeken teljesen mellőzi
az ésszerűséget.

- Kontraszt arányok javítása: Problémás a navigáció, a lábléc és a hero szekció,
illetve minden más felület, amely 200/300-as betűvastagságot és/vagy
egymáshoz közeli háttér- és szövegsízeneket használ.
- Közösségi média ikonok eltávolítása a hero szekcióból. Ez megtalálható a
láblécben és a Magyar Nemzeti Bank nem egy közösségi élményre épülő szervezet,
ahol kiemelt fontosságú a közösségi média ikonok megjelenítése. Ezzel szemben
ezek fontos helyet vesznek el a tartalomtól.
- Ikon-gombok méreteinek egységesítése/javítása. Használhatósági szempontból a
mobilverzió nem felel meg az iparági követelményeknek, mert az ikon-gombok túl
kis méretűek.
- A kenyérmorzsa navigáció elhelyezése nem megfelelő, kettévágja a tartalmat.
Ezt a hero szekció tetejére kellene elhelyezni, még a tartalom előttre.
- Annak ellenére, hogy a látványtervet nagy méretű eszközökre tervezték (1440+),
a használt betűméretek egyáltalán nem felelnek meg az iparági elvárásoknak.
Számos helyen 12 és 14 pixeles betűméretet használ a látványterv, melyek túl
kicsik és olvashatatlanok olyanok számára, akik valamilyen látási nehézséggel
küzdenek. Érdemes lenne egységesen 16 pixelben minimalizálni a betűk méretét és
csak néhány explicit esetben használni ennél kisebbet (pl. űrlapelemekhez
mellékelt tippek, hibaüzenetek).
- Betűtípusokból rengeteg félét használ a látvány, célszerű lenne kevesebbet
használni. Javasolt lenne egy cím típus (vastag talpas), egy szöveg és félkövér
típus (normál, félkövér talpatlan).

Az összes fenti változtatás alkalmazása nem tenné tönkre az oldalt látvány
szempontjából, viszont az oldal végeredményben hozzáférhető(bb) lenne.


# TODO

## Pages

[x] endpage-inpagenavigation (jogszabalyok.html)
[x] endpage-publication (publication.html)
[x] endpage-video (video.html)
[x] endpage-podcast (podcast.html)

## Styles

[x] components/content breakpoints
[x] jogszabalyok mobile
[x] podcast mobile
[x] publication mobile
[x] video mobile

## Components

[x] featured mobile
[x] footer mobile
[x] nav mobile
[x] side-nav mobile
[x] footer details
[x] nav
[x] nav-mobile
[x] details
[ ] search
[x] search-mobile
[ ] skip-link
[ ] focus-trap (nav)
[ ] meta (lang, desc, title, og)
[x] icons
[x] side item és nav item összevonása uj komponensbe: item (ni > in > slot, konfigurálható)
[x] si => item, ni => item, find & replace
[x] kiemelt > details (desktop: mindig nyitva)
[x] side-nav > details
[x] side-nav, nav-mob és egyéb listákból új (konfigurálható) komponens: item-list
[x] mobil menü navigáció layereinek új komponens (csak egy layer, aminek van egy close-gombja, benne lesz pl item-list)
[x] bizonyos desktop details tagek nyitva (JS-sel)

## Other

[x] meglévő regular file ikonok light-ra
[x] fa/light/microphone-alt
[x] fa/light/video
[x] fa/light/file-alt
[x] fa/light/file-excel
[x] fa/light/file-pdf
